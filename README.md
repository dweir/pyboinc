# pyboinc: BOINC Python bindings

I wrote these while at CERN as an OpenLab summer student in 2007 - that's (at the time of writing) nearly years ago! I have put them here in a git repo, with the various versions tagged, in the hope that they might be of some use to someone.

For any questions, please do not hesitate to contact me.

David Weir, March 2019 [david@saoghal.net](mailto:david@saoghal.net)

# Original README

_Actually this is the summary information in the inline documentation..._

This module provides bindings to standard and some less commonly features of the BOINC API. Each method is documented individually in docstrings, with python-specific behaviour mentioned.

- For more information about BOINC in general, please visit:
  http://boinc.berkeley.edu/
- API functions provided here include most of the Basic API:
  http://boinc.berkeley.edu/trac/wiki/BasicApi
- We also provide access to the Intermediate Upload API:
  http://boinc.berkeley.edu/trac/wiki/IntermediateUpload
- Support for the trickle messages API is forthcoming....

`Py_BuildValue` does not support `Bool`y types, so python code should test for zero or nonzero results rather than `True` or `False` when calling functions from this API.
