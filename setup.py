from distutils.core import setup, Extension

# May need to adjust these to suit your system, but a standard installation of the BOINC client from source will put things in these places
pyboinc = Extension('boinc', runtime_library_dirs=['/usr/local/lib'],
	extra_objects=['/usr/local/lib/libboinc.a', '/usr/local/lib/libboinc_api.a'],
	sources = ['boincmodule.C'],
	libraries=['stdc++', 'boinc', 'boinc_api', 'dl','crypto','ssl'],
	library_dirs=['/usr/local/lib/'],
	include_dirs=['/usr/local/include/boinc/'],
	extra_compile_args=['-fPIC'],extra_link_args=['-fPIC'])

setup (name = 'PyBoinc',
       version = '0.3.2',
       description = 'Basic python bindings for BOINC network computing package',
       ext_modules = [pyboinc])
